

# IB & LU port in depth

## Intro

We'll briefly review how a gateway wraps WAVES based tokens for the Ethereum.

## Lock & Issue

```js
@Callable(i)
func createTransferWrapRq(receiver: String) = {
    let pmt = value(i.payment)
    let rqId = toBase58String(i.transactionId)
    if (pmt.assetId != assetId) 
        then throw("invalid asset in payments")
    else {
        WriteSet(
            [ 
                DataEntry(FirstRqKey, if (firstRq == "") then rqId else firstRq),
                DataEntry(LastRqKey, rqId),

                DataEntry(getNextRqKey(lastRq), rqId),
                DataEntry(getPrevRqKey(rqId), lastRq),
            
                DataEntry(getRqAmountKey(rqId), pmt.amount),
                DataEntry(getRqStatusKey(rqId), NEW),
                DataEntry(getRqReceiverKey(rqId), receiver),
                DataEntry(getRqTypeKey(rqId), LOCKTYPE)
            ]
        )
    }
}
```

[luport.ride](https://github.com/Gravity-Tech/gateway/blob/main/contracts/waves/luport.ride)

A Cross-chain transfer process begins with the funds lock on the origin chain. Here, we persist a request data to the LU port state on the WAVES blockchain.

## Gravity Node & Oracle duty

Once the request is created, it'll soon be registered by an extractor, in case a gravity oracle calls it.

```go
case state.CommitSubRound:
		if roundState.commitHash != nil {
			return nil
		}
		_, err := node.gravityClient.CommitHash(node.chainType, node.nebulaId, int64(intervalId), int64(pulseId), node.oraclePubKey)
		if err != nil && err != gravity.ErrValueNotFound {
			return err
		} else if err == nil {
			return nil
		}

		data, err := node.extractor.Extract(ctx)
		if err != nil && err != extractor.NotFoundErr {
			return err
		} else if err == extractor.NotFoundErr {
			return nil
		}

		if data == nil {
			return nil
		}

		commit, err := node.commit(data, intervalId, pulseId)
		if err != nil {
			return err
		}

		roundState.commitHash = commit
		roundState.data = data
```

Here, after the successful oracle node start, a *data extraction*  takes place during the *CommitSubRound*. This process is called *commiting*. 

```go
const (
	CommitSubRound SubRound = iota
	RevealSubRound
	ResultSubRound
	SendToTargetChain

	SubRoundCount = 4

	RoundInterval = 2
)
```

After the successful *commit*,  there goes *the Reveal*  and *the Result* rounds. And only after all of these rounds, *the Pulse* is sent to target chain (Ethereum in our case) by *the Nebula*. Basically, a pulse represents the events that occured in the Gravity network. A *pulse* may be empty as well. 

```go
txId, err := node.adaptor.AddPulse(node.nebulaId, pulseId, oracles, roundState.resultHash, ctx)
if err != nil {
    return err
}

if txId != "" {
    err = node.adaptor.WaitTx(txId, ctx)
    if err != nil {
        return err
    }

    fmt.Printf("Result tx id : %s\n", txId)

    roundState.isSent = true

    err = node.adaptor.SendValueToSubs(node.nebulaId, pulseId, roundState.resultValue, ctx)
    if err != nil {
        return err
    }
}
```

Once *the pulse* is sent, *the Nebula* sends values to the *Subscribers*.

```go
func (adaptor *EthereumAdaptor) SendValueToSubs(nebulaId account.NebulaId, pulseId uint64, value *extractor.Data, ctx context.Context) error {
	var err error

	nebula, err := ethereum.NewNebula(common.BytesToAddress(nebulaId.ToBytes(account.Ethereum)), adaptor.ethClient)
	if err != nil {
		return err
	}

	ids, err := nebula.GetSubscribersIds(nil)
	if err != nil {
		return err
	}

	for _, id := range ids {
		t, err := nebula.DataType(nil)
		if err != nil {
			return err
		}

		transactOpt := bind.NewKeyedTransactor(adaptor.privKey)
		switch SubType(t) {
		case Int64:
			v, err := strconv.ParseInt(value.Value, 10, 64)
			if err != nil {
				return err
			}
			_, err = nebula.SendValueToSubInt(transactOpt, v, big.NewInt(int64(pulseId)), id)
			if err != nil {
				return err
			}
		case String:
			_, err = nebula.SendValueToSubString(transactOpt, value.Value, big.NewInt(int64(pulseId)), id)
			if err != nil {
				return err
			}
		case Bytes:
			println(value.Value)
			v, err := base64.StdEncoding.DecodeString(value.Value)
			if err != nil {
				return err
			}

			_, err = nebula.SendValueToSubByte(transactOpt, v, big.NewInt(int64(pulseId)), id)
			if err != nil {
				continue
			}
		}
	}
	return nil
}
```

Here is the implementation for the Ethereum chain. We instantiate *a Nebula* for Ethereum, and deserialize the request registered by the WAVES LU port. A *Gravity Extractor* is responsible for serializing the data. In our case, we have a *Bytes* subscription data type and the branch *Bytes* is handled.

```js
function sendValueToSubByte(bytes memory value, uint256 pulseId, bytes32 subId) public {
  sendValueToSub(pulseId, subId);
  ISubscriberBytes(subscriptions[subId].contractAddress).attachValue(value);
}
```

[Nebula.sol](https://github.com/Gravity-Tech/gravity-core/blob/master/contracts/ethereum/Nebula/Nebula.sol)

 After that, the **sendValueToSubByte** mtehod is called. It confirms that the *pulseId* is being handled once, instantiates the subscriber and invokes the **attachValue** on it. 

Let's see what happens if the **subscriptions[subId].contractAddress** is deployed the *IB Port* address.

```js
function attachValue(bytes calldata value) override external {
    require(msg.sender == nebula, "access denied");
    for (uint pos = 0; pos < value.length; ) {
        bytes1 action = value[pos]; pos++;

        if (action == bytes1("m")) {
            uint swapId = deserializeUint(value, pos, 32); pos += 32;
            uint amount = deserializeUint(value, pos, 32); pos += 32;
            address receiver = deserializeAddress(value, pos); pos += 20;
            mint(swapId, amount, receiver);
            continue;
        }

        if (action == bytes1("c")) {
            uint swapId = deserializeUint(value, pos, 32); pos += 32;
            RequestStatus newStatus = deserializeStatus(value, pos); pos += 1;
            changeStatus(swapId, newStatus);
            continue;
        }
        revert("invalid data");
    }
}
```


We see, that it either confirms a request status or mints a wrapped token. 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ1MjQ2NzU4LC0yMTI0ODQ3Mjk5LC00NT
cxOTQ4MzddfQ==
-->